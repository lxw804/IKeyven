package _stsg;

import java.util.ArrayList;

import org.ansj.splitWord.analysis.ToAnalysis;
import _stsg.util.*;

public class Stsgcom {
	public String compression(String str, String rulefile, boolean log) {
		// 开始缩句
		ParseTree pt = new ParseTree();
		_ansj.split.IToken sw = _ansj.split.TokenFactory.getInstance(str);
		Node sroot = pt.createTree(sw.getList()); // 原句建树
		if (log) {
			_fileutil.Log.log("语法树:" + pt.parseTree);
		}
		// 取到原句建树中的所有结点Node
		TreeParser facility = new TreeParser(sroot);
		ArrayList<Node> allNodes = facility.list;
		// 开始比对规则
		Stsgml sm = new Stsgml();
		int cnt = 0;
		while (true) {
			boolean canExit = true;
			for (int i = 0; i < allNodes.size() - sw.getList().size(); i++) {
				// 导入规则
				sm.rulesTreeList.clear();
				sm.ImportRules(rulefile);
				ArrayList<Rule> rlist = sm.rulesTreeList;
				for (Rule r : rlist) {
					if (RuleParser.isEqualTree(allNodes.get(i), r.source)) {
						facility.Visit(allNodes.get(i), r.source);
						facility.Visit_Target(r.target);
						allNodes.get(i).replace(r.target);
						canExit = false;
						if(log) {
							_fileutil.Log.log("规则:");
							_fileutil.Log.log(r.sr);
							_fileutil.Log.log(r.tr);
						}
						break;
					}
				}
			}
			if (canExit) {
				break;
			}
			cnt++;
			if (cnt >= 100) {
				_fileutil.Log.log("缩句失败!");
				_fileutil.Log.log("句子:" + str);
				break;
			}
		}
		OutputResult outans = new OutputResult();
		outans.visit(sroot);
		return outans.toString();
	}

	public static void main(String[] args) {
		ToAnalysis.parse("");
		Stsgcom test = new Stsgcom();
		_init.StanfordParser.getInstance().setModel("lib/chinesePCFG.ser.gz");
		System.out.println(test.compression("有2米左右的布匹吗？", "F:/2.txt", true));
	}
}
