package _trunk;

import java.io.IOException;
import java.util.*;

public class DepRules {
	public List<_trunk.util.Rule> rules = null;

	public List<_trunk.util.Rule> reader(String deprulestxt) {
		this.rules = new ArrayList<>();
		
		try {
			List<String> list = _fileutil.FileUtil.readFile(deprulestxt, "gbk");
			_trunk.util.Rule r = null;
			for(String str : list) {
				if(str.matches("begin.*")) {
					r = new _trunk.util.Rule();
					this.rules.add(r);
					continue;
				}
				// nsubj .* .* 4 �� VE 2
				_trunk.util.Edge e = new _trunk.util.Edge();
				String[] temp = str.split(" ");
				e.edgename = temp[0];
				e.depword = temp[1];
				e.depinfo = temp[2];
				e.depId = Integer.valueOf(temp[3]);
				e.govword = temp[4];
				e.govinfo = temp[5];
				e.govId = Integer.valueOf(temp[6]);
				r.add(e);
			}
		} catch (IOException e) {
			_fileutil.Log.log("�����������ȡʧ��:");
			_fileutil.Log.log(e.getMessage());
			e.printStackTrace();
		}
		
		return this.rules;
	}
	
	public static void main(String[] args) {
		DepRules test = new DepRules();
		test.reader("src/_IKeyven/deprules.txt");
		for(_trunk.util.Rule r : test.rules) {
			System.out.println(r);
		}
	}
}
