package _trunk;

import _trunk.util.Edge;

public class Gotrunk {
	public static String gotrunk(DepTree a, DepRules b, boolean log) {
		String str = "";

		Matcher test = new Matcher();
		boolean isOk = false;
		for (_trunk.util.Rule rr : b.rules) {
			if (test.check(a.edges, rr.r, log)) {
				String[] temp = new String[rr.r.size()];
				for (Edge e : rr.r) {
					if (e.depId < 0) {
						continue;
					}
					temp[e.depId - 1] = a.edges.get(test.map3.get(e.depId) - 1).depword;
				}
				for (String t : temp) {
					if (t == null) {
						break;
					}
					str += t;
				}
				isOk = true;
				break;
			}
		}
		if (isOk == false) {
			str = a.str;
		}

		return str;
	}

	public static void main(String[] args) {
		DepRules test1 = new DepRules();
		test1.reader("src/_IKeyven/deprules.txt");
		_IKeyven.Function.setModel("lib/chinesePCFG.ser.gz",
				"src/_IKeyven/userwords.txt");
		System.out.println("白色的三星你们这儿有没有？");
		DepTree test2 = new DepTree("白色的三星你们这儿有没有？");
		System.out.println(Gotrunk.gotrunk(test2, test1, true));
	}
}
